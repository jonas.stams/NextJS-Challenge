
## NextJS-Challenge

This is a coding challenge I recently did for a company. I thought it would be good to share it for other companies to have a look at how I solve something in Next.js

### Assignment
You are asked to develop a simple e-commerce page with the following:

#### Requirements:
- User can browse 8 products in a grid
- User can open/close the cart with the cart button - cart trigger should include an animation
- User can add a product to the cart on click of the product. Cart should open each time a product is added and the product should be listed in the cart
- User can remove a product from the cart
- User can see the total of the cart
- User can checkout on click of the checkout button. Display a confirmation alert

#### Guidelines:
- setup a new next js project
- setup and use the mantine ui Library with the next js project, following the recommend methods in the mantine site - https://mantine.dev/
- use and customize the needed components to complete the page according to the mockups (see Figma link below)
- make sure the design is responsive and work well with mobile and desktop (no mockups for mobile - your judgment)
- the project file structure is taken into consideration
- make sure that the animation is adapted to the mobile version, this can mean changing some details about the animation
- deploy the app into vercel, and push the code into a GitHub or bitbucket repo
- make a next js api endpoint that receive the checkout form data

#### Bonus:
- make a next js api endpoint that receive the checkout form data, validate it another time, and save it to Firestore - if the api return 200 success show a success notification in the front

#### Figma design:
![alt text](./CODE-CHALLENGE-DESIGN.png)

