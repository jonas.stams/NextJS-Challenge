import {fireEvent, render, RenderResult, screen, waitFor} from '@testing-library/react'
import '@testing-library/jest-dom'
import {Home} from "../Home/Home";
import {MockProducts} from "../../__mock__/products";
import {CartContextProvider} from "../../context/CartContext";
import {ExchangeRateContextProvider} from "../../context/ExhangeRatesContext";
import * as ShopService from "../../services/ShopService";
import {AxiosResponse} from "axios";
import * as MantineNotifications from "@mantine/notifications";

jest.mock('./../../services/ExchangeService', () => ({
        getEthExchangeRates: jest.fn(() => Promise.resolve({USD: 0}))
    })
);

jest.mock('./../../services/ShopService', () => ({
        placeOrder: jest.fn(),
        getProducts: jest.fn(),
    })
);
const wrapper = async (): Promise<RenderResult> => {
    return render(
        <CartContextProvider>
            <ExchangeRateContextProvider>
                <Home/>
            </ExchangeRateContextProvider>
        </CartContextProvider>
    );

}
describe('Home', () => {

    it('renders correctly', async () => {
        const {container} = await wrapper();
        expect(container).toMatchSnapshot()
    })

    it('renders the heading', () => {
        wrapper();
        const heading = screen.getByRole('heading', {
            name: /Collection #1/i,
        })
        expect(heading).toBeInTheDocument()
    })

    it('renders products', () => {
        wrapper();
        const product1 = screen.getByText('Product #1')
        expect(product1).toBeInTheDocument()
    })

    describe('WHEN product is added to cart', () => {
        it('opens cart', () => {
            wrapper();
            const product1 = screen.getByText('Product #1');
            fireEvent.click(product1);
            const cartTitle = screen.getByText('My cart')
            expect(cartTitle).toBeInTheDocument()
        })

        it('calculates totals correctly', () => {
            wrapper();
            const product1 = screen.getByText('Product #1');
            const product2 = screen.getByText('Product #2');

            fireEvent.click(product1);
            fireEvent.click(product2);

            const totalEth = screen.getByTestId('total-eth');
            expect(totalEth.textContent).toEqual('0.090');
            const totalUsd = screen.getByTestId('total-usd');
            expect(totalUsd.textContent).toEqual('$0.00')
        })
    })

    it('shows notification on good checkout', async () => {
        await wrapper();
        const product1 = screen.getByText('Product #1');
        fireEvent.click(product1);
        const notificationSpy = jest.spyOn(MantineNotifications, 'showNotification');
        jest.spyOn(ShopService, 'placeOrder').mockResolvedValue({
            status: 200,
            data: {data: {totalPrice: 100}}
        } as AxiosResponse);
        const button = screen.getByTestId('checkout-btn');
        fireEvent.click(button);

        await waitFor(() => {
            expect(notificationSpy).toBeCalledTimes(1);
        })
    })

    it('shows notification on bad checkout', async () => {
        await wrapper();
        const product1 = screen.getByText('Product #1');
        fireEvent.click(product1);
        const notificationSpy = jest.spyOn(MantineNotifications, 'showNotification');
        jest.spyOn(ShopService, 'placeOrder').mockResolvedValue({
            status: 400,
            data: {data: {totalPrice: 100}}
        } as AxiosResponse);
        const button = screen.getByTestId('checkout-btn');
        fireEvent.click(button);

        await waitFor(() => {
            expect(notificationSpy).toBeCalledTimes(1);
        })
    })


})
