import {createStyles} from "@mantine/core";

export const useStyles = createStyles((theme) => ({
        page: {
            display: 'flex',
            minHeight: '100vh',
            '& .price': {
                display: 'flex',
                alignItems: 'center',

                '& > div': {
                    paddingTop: '4px'
                }
            }
        },
        shopWrap: {
            position: 'relative',
            flex: 1,
            padding: '50px 35px',


        }
    }))
;

