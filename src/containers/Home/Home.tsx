import React, {useCallback} from 'react';
import {useStyles} from "./Home.styles";
import ProductCard from "../../components/ProductCard/ProductCard";
import {Box, Grid, Space, Title} from '@mantine/core';
import Cart from "../../components/Cart/Cart";
import {placeOrder} from "../../services/ShopService";
import {NextPage} from "next";
import {useCartContext} from "../../context/CartContext";
import {showNotification} from "@mantine/notifications";
import {MockProducts} from "../../__mock__/products";


export const Home: NextPage = () => {
    const {classes} = useStyles();
    const {items, reset, setIsLoading} = useCartContext();

    const renderProducts = useCallback(
        (): JSX.Element[] => {
            return MockProducts.map((product, i) => (
                <Grid.Col key={`product${i}`} sm={12} md={6} lg={3}>
                    <ProductCard product={product}/>
                </Grid.Col>
            ));
        }, []);

    const checkout = async () => {
        setIsLoading(true);
        const {status, data} = await placeOrder({products: items.map(({product}) => product.id)});
        if (status !== 200) {
            setIsLoading(false);
            return showNotification({title: 'Order not placed', message: data.error, color: 'red'});
        }

        showNotification({
            title: `Order placed!`,
            message: `Your order of ${data.data.totalPrice || 0}ETH has been placed.`,
            color: 'green'
        })
        reset();
    }

    return (
        <Box className={classes.page}>
            <Box className={classes.shopWrap}>
                <Title order={1}>Collection #1</Title>
                <Space h={'xl'}/>
                <Grid>
                    {renderProducts()}
                </Grid>
            </Box>
            <Cart onCheckout={checkout}/>
        </Box>
    );
};
