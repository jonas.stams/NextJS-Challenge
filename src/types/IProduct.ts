export interface IProduct {
    id: number;
    name: string;
    price: number;
    collection: number;
}
