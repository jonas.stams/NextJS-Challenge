import React, {FunctionComponent, useMemo} from 'react';
import {Card, Flex, Text} from "@mantine/core";
import {ProductCardProps} from "./ProductCard.types";
import {useStyles} from './ProductCard.styles';
import Icon from '../Icon/Icon';
import {useCartContext} from "../../context/CartContext";
import classNames from "classnames";

const ProductCard: FunctionComponent<ProductCardProps> = ({product}) => {
    const {classes} = useStyles();
    const {addProduct, items} = useCartContext();

    const isSelected = useMemo(() => {
        const isInCart = items.find(x => x.product.id === product.id);
        return !!isInCart;
    }, [items, product]);

    const addToCart = () => {
        if (isSelected) return;
        addProduct(product);
    }

    return (
        <Card className={classNames(classes.card, {selected: isSelected})} radius="md"
              onClick={addToCart}>
            <Card.Section className={'img-placeholder'}/>

            <Flex justify={'space-between'} align={'center'} className={'details'}>
                <Text weight={500} component="a" size={'sm'}>
                    {product.name}
                </Text>
                <Flex align={'center'}>
                    <Icon name={'eth'}/>
                    <Text weight={500} size={'sm'}>
                        {product.price}
                    </Text>
                </Flex>

            </Flex>

        </Card>
    );
};

export default ProductCard;
