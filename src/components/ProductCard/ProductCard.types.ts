import {IProduct} from "../../types/IProduct";

export interface ProductCardProps {
    product: IProduct;
}
