import {createStyles} from "@mantine/core";

export const useStyles = createStyles((theme) => ({
    card: {
        background: '#FAFAFA',
        boxShadow: theme.shadows.sm,
        borderRadius: '20px',
        cursor: 'pointer',
        transition: 'transform .2s',

        '&.selected': {
          opacity: 0.2
        },
        '&:hover': {
            boxShadow: theme.shadows.md,
            transform: 'scale(1.02)',
        },

        '& .img-placeholder': {
            height: '195px',
            backgroundColor: '#D9D9D9'
        },

        '& .details': {
            padding: '1rem 0 0',
            '& .price': {
                display: 'flex',
                alignItems: 'center',

                '& > div': {
                    paddingTop: '4px'
                }
            }
        }
    }
}));
