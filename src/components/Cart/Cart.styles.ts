import {createStyles} from "@mantine/core";

export const useStyles = createStyles((theme) => ({
    cartWrap: {
        backgroundColor: theme.colors.gray[1],
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 24,
        position: 'relative',
        boxShadow: 'inset -2px 0px 8px rgba(0, 0, 0, 0.1)',


        '&.open': {
            justifyContent: 'space-between',
            flexDirection: 'column',
        },

        [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
            backgroundColor: 'transparent',
            position: 'fixed',
            right: 0,
            top: 0,
            bottom: 0,
            maxWidth: 0,
            boxShadow: 'unset',

            '&.open': {
                backgroundColor: theme.colors.gray[1],
                maxWidth: 'unset',
                boxShadow: 'inset -2px 0px 8px rgba(0, 0, 0, 0.1)',
            }
        },
    },
    cartButton: {
        position: 'sticky',
        top: 24,
        height: 46,
        width: 46,
        background: '#FFFFFF',
        boxShadow: '0px 4px 8px rgba(31, 15, 46, 0.08)',
        borderRadius: 8,
        padding: 0,

        '&:hover': {
            background: '#ededed'
        },

        '& svg': {
            minWidth: 15
        },

        [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
            position: 'absolute',
            right: 20,
            top: 20,
        },


    },
    priceDetails: {
        textAlign: 'right'
    }
}));

