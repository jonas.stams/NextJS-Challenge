import React, {FunctionComponent, useCallback} from 'react';
import {motion} from "framer-motion";
import classNames from "classnames";
import {Box, Button, Flex, Stack, Text} from "@mantine/core";
import Icon from "../Icon/Icon";
import CartItem from "../CartItem/CartItem";
import {useStyles} from "./Cart.styles";
import {useCartContext} from "../../context/CartContext";
import {useExchangeRateContext} from "../../context/ExhangeRatesContext";
import {CartProps} from "./Cart.types";

const Cart: FunctionComponent<CartProps> = ({onCheckout}) => {
    const {classes} = useStyles();
    const {toggle, isOpen, items, totalPrice, isLoading} = useCartContext();
    const {convertToUsd} = useExchangeRateContext()

    const renderCartItems = useCallback(
        (): JSX.Element => {
            if (!isOpen) return <></>;

            return (
                <Stack>
                    {items.map(x => (
                        <CartItem key={x.product.id} item={x}/>
                    ))}
                </Stack>
            )
        },
        [items, isOpen],
    );


    return (
        <motion.div className={classNames(classes.cartWrap, {open: isOpen})} animate={{width: isOpen ? 285 : 95}}>
            <Stack w={'100%'}>
                <Flex justify={'space-between'} align={'center'}>
                    {isOpen && <Text size={'lg'} weight={600}>My cart</Text>}

                    <Button onClick={toggle} className={classes.cartButton}>
                        <Icon name={'shopping_cart'}/>
                    </Button>
                </Flex>
                {renderCartItems()}
            </Stack>
            {isOpen && <Stack w={'100%'}>
                <Flex justify={'space-between'}>
                    <Text weight={600} size={'xl'}>Total</Text>
                    <Box className={classes.priceDetails}>
                        <Flex justify={'flex-end'}>
                            <Icon name={'eth'}/>
                            <Text weight={600} size={'md'} data-testid={'total-eth'}>{totalPrice.toFixed(3)}</Text>
                        </Flex>
                        <Text size={'sm'} color={'gray.7'} data-testid={'total-usd'}>${convertToUsd(totalPrice).toFixed(2)}</Text>
                    </Box>
                </Flex>
                <Button loading={isLoading} disabled={totalPrice === 0} color={'dark'} data-testid={'checkout-btn'} onClick={onCheckout}>Complete checkout</Button>
            </Stack>
            }
        </motion.div>
    );
};

export default Cart;
