import {createStyles} from "@mantine/core";

export const useStyles = createStyles((theme) => ({
        imgPreview: {
            position: 'relative',
            backgroundColor: theme.colors.bgGray,
            width: 40,
            height: 40,
            borderRadius: theme.radius.md
        },
        closeButton: {
            position: 'absolute',
            maxWidth: 20,
            maxHeight: 20,
            right: -10,
            top: -10,
            borderRadius: theme.radius.xl,
            fontSize: theme.fontSizes.sm,
            boxShadow: theme.shadows.md

        },
        priceDetails: {
            textAlign: 'right',
            flex: 1
        }
    }))
;

