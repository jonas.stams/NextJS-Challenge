import {ICartItem} from "../../types/ICartItem";

export interface CartItemProps {
    item: ICartItem;
}
