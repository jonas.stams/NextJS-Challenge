import {Box, CloseButton, Flex, Text} from '@mantine/core';
import React, {FunctionComponent} from 'react';
import {CartItemProps} from "./CartItem.types";
import {useStyles} from "./CartItem.styles";
import {useExchangeRateContext} from "../../context/ExhangeRatesContext";
import Icon from "../Icon/Icon";
import {useCartContext} from "../../context/CartContext";


const CartItem: FunctionComponent<CartItemProps> = ({item}) => {
    const {classes} = useStyles();
    const {convertToUsd} = useExchangeRateContext();
    const {removeProduct} = useCartContext();
    const {product} = item;
    return (
        <Flex w={'100%'} justify={'stretch'} align={'center'} gap={'md'} key={product.id}>
            <Box className={classes.imgPreview}>
                <CloseButton onClick={() => removeProduct(product)} className={classes.closeButton} size={'sm'}
                             bg={'gray.0'} color={'gray.5'}/>
            </Box>
            <Box>
                <Text weight={600} size={'sm'}>{product.name}</Text>
                <Text size={'sm'}>{`Collection #${product.collection}`}</Text>
            </Box>
            <Box className={classes.priceDetails}>
                <Flex justify={'flex-end'}>
                    <Icon name={'eth'}/>
                    <Text weight={600} size={'sm'}>{item.product.price}</Text>
                </Flex>
                <Text size={'xs'} color={'gray.7'}>${convertToUsd(product.price).toFixed(2)}</Text>
            </Box>
        </Flex>
    );
};

export default CartItem;
