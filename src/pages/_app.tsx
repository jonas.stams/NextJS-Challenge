import '../styles/globals.css'
import type {AppProps} from 'next/app'
import {MantineProvider} from "@mantine/core";
import {NotificationsProvider} from "@mantine/notifications";
import Head from 'next/head';
import {CartContextProvider} from "../context/CartContext";
import {ExchangeRateContextProvider} from "../context/ExhangeRatesContext";

export default function App({Component, pageProps}: AppProps) {
    return (
        <>
            <Head>
                <title>Agora NFT Marketplace</title>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
            </Head>

            <MantineProvider withGlobalStyles withNormalizeCSS
                             theme={{
                                 colorScheme: 'light',
                                 fontFamily: 'Metropolis',
                                 colors: {
                                     'bgGray': ['#D9D9D9']
                                 }
                             }}>
                <NotificationsProvider>
                    <ExchangeRateContextProvider>
                        <CartContextProvider>
                            <Component {...pageProps} />
                        </CartContextProvider>
                    </ExchangeRateContextProvider>

                </NotificationsProvider>
            </MantineProvider>
        </>
    )
}
