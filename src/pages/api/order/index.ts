import type {NextApiRequest, NextApiResponse} from 'next'
import {db} from '../../../api/database/Firebase';
import {MockProducts} from "../../../__mock__/products";

type Data = {
    data?: any;
    error?: string;
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    if (req.method !== 'POST') {
        return res.status(405).json({error: 'Only POST is allowed'});
    }

    const {body} = req;
    const products = MockProducts;
    const existingProductIds = products.map(x => x.id);

    if (!body.products) {
        return res.status(400).json({error: 'Your order payload is not correct.'});
    }

    if (!body.products || !body.products.every((id: number) => existingProductIds.includes(id))) {
        return res.status(400).json({error: 'There are unknown products in your order.'});
    }

    const totalPrice = products
        .filter(({id}) => body.products.includes(id))
        .reduce((total, {price}) => total + price, 0)

    const {id} = await db.collection('orders').add({createdAt: new Date(), products: body.products, totalPrice});

    res.status(200).json({
        data: {
            orderId: id,
            products: body.products,
            totalPrice
        }
    })

}
