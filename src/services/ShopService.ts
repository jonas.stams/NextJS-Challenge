import axios, {AxiosResponse} from "axios";
import {OrderPayload} from "../types/OrderPayload";

export const placeOrder = async (payload: OrderPayload): Promise<AxiosResponse> => {
    return await axios.post('/api/order', payload);
}
