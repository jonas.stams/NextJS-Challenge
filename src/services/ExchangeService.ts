import axios from "axios";

export const getEthExchangeRates = async (): Promise<Record<string, string>> => {
    const {data} = await axios.get('https://api.coinbase.com/v2/exchange-rates?currency=ETH');
    return data.data.rates;
}
