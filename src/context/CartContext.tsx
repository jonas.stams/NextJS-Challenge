import {
    createContext,
    Dispatch,
    FunctionComponent,
    ReactNode,
    SetStateAction,
    useCallback,
    useContext,
    useMemo,
    useState
} from "react";
import {ICartItem} from "../types/ICartItem";
import {IProduct} from "../types/IProduct";

export interface CartContextData {
    items: ICartItem[],
    setItems: Dispatch<SetStateAction<ICartItem[]>>,
    isOpen: boolean,
    setIsOpen: Dispatch<SetStateAction<boolean>>,
    isLoading: boolean,
    setIsLoading: Dispatch<SetStateAction<boolean>>
}

export const CartContext = createContext<CartContextData>({} as CartContextData);

export const CartContextProvider: FunctionComponent<{ children: ReactNode }> = ({children}) => {
    const [items, setItems] = useState<ICartItem[]>([]);
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    return (
        <CartContext.Provider
            value={{
                items,
                setItems,
                isOpen,
                setIsOpen,
                isLoading,
                setIsLoading
            }}>
            {children}
        </CartContext.Provider>
    );
};

export const useCartContext = () => {
    const {items, setItems, isOpen, setIsOpen, isLoading, setIsLoading} = useContext(CartContext);

    const addProduct = (product: IProduct) => {
        const existing = items.find(x => x.product.id === product.id);

        if (!existing) {
            setItems([...items, {product, quantity: 1}]);
            open();
        }
    };

    const removeProduct = (product: IProduct) => {
        setItems(currItems => currItems.filter(x => x.product.id !== product.id));
    };

    const totalPrice = useMemo(() => items
        .map(({product, quantity}) => product.price * quantity)
        .reduce((a, b) => a + b, 0), [items]);

    const open = useCallback(
        () => {
            setIsOpen(true)
        }, [setIsOpen],
    );
    const close = useCallback(
        () => {
            setIsOpen(false)
        }, [setIsOpen],
    );

    const toggle = useCallback(
        () => {
            setIsOpen(open => !open);
        },
        [setIsOpen],
    );

    const reset = () => {
        setIsLoading(false);
        setItems([]);
    }


    return {
        items,
        addProduct,
        removeProduct,
        totalPrice,
        isOpen,
        open,
        close,
        toggle,
        reset,
        isLoading,
        setIsLoading
    };
};
