import {
    createContext,
    FunctionComponent,
    ReactNode,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useState
} from "react";
import {getEthExchangeRates} from "../services/ExchangeService";

export interface ExchangeRateContextData {
    rates?: Record<string, string>,
}

export const ExchangeRateContext = createContext<ExchangeRateContextData>({} as ExchangeRateContextData);


export interface ExchangeRateContextProviderProps {
    children: ReactNode
}

export const ExchangeRateContextProvider: FunctionComponent<ExchangeRateContextProviderProps> = ({
                                                                                                     children,
                                                                                                 }) => {
    const [rates, setRates] = useState<Record<string, string>>();

    useEffect(() => {
        // Could possibly add an interval here to get new rates every x seconds.
        (async () => {
            const currentRates = await getEthExchangeRates();
            setRates(currentRates)
        })()
    }, []);


    return (
        <ExchangeRateContext.Provider
            value={{
                rates,
            }}>
            {children}
        </ExchangeRateContext.Provider>
    );
};

export const useExchangeRateContext = () => {
    const {rates} = useContext(ExchangeRateContext);

    const usdExchangeRate = useMemo((): number => {
        if (!rates) return 0;
        return +rates.USD;
    }, [rates]);

    const convertToUsd = useCallback((ethPrice: number) => ethPrice * usdExchangeRate, [usdExchangeRate]);

    return {convertToUsd};
};
