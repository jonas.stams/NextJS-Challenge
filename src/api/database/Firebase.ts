import admin from 'firebase-admin';
import {FirebaseServiceAccount} from "./config";

if (!admin.apps.length) {
    try {
        admin.initializeApp({
            credential: admin.credential.cert(FirebaseServiceAccount)
        });
    } catch (error) {
        console.log('Firebase admin initialization error', error);
    }
}

export const db = admin.firestore();
